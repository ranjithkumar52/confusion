//gulp tasks - code based
//gulps operates through stream objects
"use strict"; 

//loading plugins
var gulp = require("gulp"), 
	sass = require("gulp-sass"), 
	browserSync = require("browser-sync"),
	del = require("del"),
	imagemin = require("gulp-imagemin"), 
	uglify = require("gulp-uglify"),
	usemin = require("gulp-usemin"), 
	rev = require("gulp-rev"),
	cleanCss = require("gulp-clean-css"), 
	flatmap = require("gulp-flatmap"), 
	htmlmin = require("gulp-htmlmin");
	
//setting up tasks
//sass tasks
gulp.task("sass", function(){ 
//takes file globs and creates a stream of objects that represents the below files 
//allows the stream to be piped through a function
//specifies the destination of the changed files
	return gulp.src("./css/*.sass") 
	.pipe(sass().on("error", sass.logError))
	.pipe(gulp.dest("./css")); 
}); 
//sass:watch task
gulp.task("sass:watch", function(){
	gulp.watch("./css/*.scss", ["sass"]); //if there is any changes in the scss file then perform sass task automatically
}); 

//browser-sync task
gulp.task("browser-sync", function(){
	var files = [ //files is js object of type array
		"./*.html",
		"./css/*.css",
		"./img/*.{png, jpg, gif}", 
		"./js/*.js"
	];
	
	browserSync.init(files, {
		//options
		server: {
			baseDir: "./"
		}
	});
});


gulp.task("default", ["browser-sync"], function(){
	gulp.start("sass:watch");
});

//cleaning dist folder; clean task
gulp.task("clean", function(){
	return del(["dist"]);
});

//copying fonts into dist folder
gulp.task("copyfonts", function(){
	gulp.src("./node_modules/font-awesone/fonts/**/*.{ttf, woff, eof, svg}*")
	.pipe(gulp.dest("./dist/fonts"));
});

//image min and compressing
gulp.task("imagemin", function(){
	return gulp.src("img/*{png, jpg, gif}")
	.pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
	.pipe(gulp.dest("dist/img"));
});

//usemin task; takes the html files and looks up the css and js block and perform various functions to it in the dist folder
gulp.task("usemin", function(){
	return gulp.src("./*.html")
	.pipe(flatmap(function(stream, file){
		return stream
		.pipe(usemin({
			css: [rev()], 
			html: [function(){return htmlmin({collapseWhitespace: true})}], 
			js: [uglify(), rev()], 
			inlinejs: [uglify()], 
			inlinecss: [cleanCss(), "concat"]
		}))
	}))
	.pipe(gulp.dest("dist/"))
});


//build tasks; perform clean first and then build; then usemin
gulp.task("build", ["clean"], function(){
	gulp.start("copyfonts", "imagemin", "usemin");
});
































