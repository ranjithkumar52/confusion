'use strict'

module.exports = function(grunt) {

  require('time-grunt')(grunt);

  require('jit-grunt')(grunt, {
	  //to link with usemin
	  useminPrepare: "grunt-usemin"
  });

  grunt.initConfig({
    //various grunt tasks
    sass: {
      dist: {
        files: {
          'css/styles.css': 'css/styles.scss'
        }
      }
    },//end sass

    watch: {
      files: 'css/*.scss',
      tasks: ['sass']
    }, //end watch

    browserSync: {
      dev: {
        bsFiles: {
          src: [
            'css/*.css',
            '*.html',
            'js/*.js'
          ]
        },
        options: {
          watchTask: true,
          server: {
            baseDir: './'
          }
        }
      }
    }, //browserSync

    copy:{
      html: {
        files: [{
          expand: true,
          dot: true,
          cwd: './',
          src: ['*.html'],
          dest: 'dist'
        }]
      },

      fonts: {
        files: [{
          expand: true,
          dot: true,
          cwd: 'node_modules/font-awesome',
          src: ['fonts/*.*'],
          dest: 'dist'
        }]
      }
    },

    clean: {
      build: {
        src: ['dist/']
      }
    },

    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: './',
          src: ['img/*.{png, gif, jpg}'],
          dest: 'dist/'
        }]
      }
    }, 
	
	useminPrepare: {
		//prepare files and concats minify
		foo: {
			//random name
			dest: 'dist', 
			src: ["contactus.html", "aboutus.html", "index.html"]
		}, 
		
		options: {
			flow: {
				steps: {
					css: ["cssmin"],
					js: ["uglify"]
				}, 
				
				post: {
					css: [{
						name: "cssmin",
						createConfig: function(context, block){
							//need to min font-awesome
							var generated = context.options.generated;
							generated.option = {
								keepSpecialComments: 0,
								rebase: false
							}; 
						}
					}]
				}
			}
		}
	}, 
	
	concat: {
		options: {
			separator: ";"
		}, 
		dist: {
			//empty
			}
	}, 
	
	uglify: {
		dist: {
			//empty
		}
	}, 
	
	cssmin: {
		dist: {
			//empty
		}
	}, 
	
	filerev: {
		//adds another extension to the file name //remember browsercache problem and how i used to change css version name
		//this is something similar to that
		options: {
			encoding: "utf8", 
			algorithm: "md5", 
			length: 20
		}, 
		release: {
			files: [{
				src: ["dist/css/*.css", "dist/js/*.js"]
			}]
		}
	}, 
	
	usemin: {
		html: ["dist/contactus.html", "dist/aboutus.html", "dist/index.html"], 
		options: {
			assetsDir: ["dist", "dist/css", "dist/js"]
		}
	}

  });

  grunt.registerTask('css', ['sass']);//execute sass task

  grunt.registerTask('default', ['browserSync', 'watch']);

  grunt.registerTask('build', [
    //maintain the sequence here, dont change the order
	'clean',
    'copy',
    'imagemin', 
	"useminPrepare",
	"concat", 
	"cssmin",  
	"uglify", 
	"filerev", 
	"usemin"
  ]);
}
