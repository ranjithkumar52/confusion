$(document).ready(function(){
  $("#mycarousel").carousel({interval: 2000}); //to slide every 2sec
  $("#carousel-button").click(function(){
      if($("#carousel-button").children("span").hasClass("fa-pause")){
        $("#mycarousel").carousel('pause');//if button is pause then contniue pause action
        $("#carousel-button").children("span").removeClass("fa-pause");
        $("#carousel-button").children("span").addClass("fa-play");
      }
      else if($("#carousel-button").children("span").hasClass("fa-play")){
        $("#mycarousel").carousel('cycle');//if button is cycle then continue sliding the carousel
        $("#carousel-button").children("span").removeClass("fa-play");
        $("#carousel-button").children("span").addClass("fa-pause");
      }
  });
});

$('#ReserveTableButton').click(function(){
  $('#ReserveTableModal').modal('toggle');
});

$('#loginButton').click(function(){
  $('#loginModal').modal('toggle');
});
